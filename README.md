# Wecasa simple test app

## What it does

  - check for bookable pros and returns all availables
  - creates an appointment if booking is validated (persistence)
  - prints a success message if booking is validated
  - prints an error message if error occured during booking validation

## What it does not

  - catch all errors (no high level error handler)
  - validate that booking is >= Time.now (you can choose a past date for booking)
  - validate that a booking is unique for a person
  - validate address format/validaty

## TODO:

  - add more test
  - add `Geocoder.configure(lookup: :test, ip_lookup: :test)` in `spec/rails_helper.rb` and stub with `Geocoder::Lookup::Test.add_stub`
  - move `ProfessionalPresenter` outside the service
  - add a custom validtor for `prestations`
  - add booking's validation for `starts_at` (must starts after `Time.now`)
  - add I18n

## Installation

### Database

```
bundle exec rake db:create db:migrate

# Database setup
bundle exec rake 'populate:all[data.json]'
```

## Server

```
bundle exec rails s
```

Then open http://localhost:3000


## Tests

```
bundle exec rspec
```

## Lint

```
bundle exec rubocop
```
