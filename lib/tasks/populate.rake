# frozen_string_literal: true

namespace :populate do
  desc 'Populate database with prestations, professionals and bookings'
  task :all, [:path] => %i[prestations professionals bookings] do
  end

  desc 'Populate database with prestations'
  task :prestations, [:path] => :environment do |_t, args|
    @path = args[:path]
    Prestation.create(json['prestations'])
  end

  desc 'Populate database with professionals'
  task :professionals, [:path] => :environment do |_t, args|
    @path = args[:path]
    Professional.create(json['pros'])
  end

  desc 'Populate database with bookings'
  task :bookings, [:path] => :environment do |_t, args|
    @path = args[:path]
    Booking.create(json['bookings'])
  end

  private

  def json
    @json ||= JSON.parse(File.read(@path))
  end
end
