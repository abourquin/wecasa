class RemoveAppointmentsFromProfessionals < ActiveRecord::Migration[5.2]
  def change
    remove_column :professionals, :appointments
  end
end
