class CreateProfessionals < ActiveRecord::Migration[5.2]
  def change
    create_table :professionals do |t|
      t.string :name
      t.string :address
      t.float :lat
      t.float :lng
      t.float :max_kilometers
      t.json :opening_hours
      t.json :appointments

      t.string :prestations, array: true

      t.timestamps
    end
  end
end
