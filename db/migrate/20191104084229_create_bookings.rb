class CreateBookings < ActiveRecord::Migration[5.2]
  def change
    create_table :bookings do |t|
      t.string :email
      t.string :name
      t.string :address
      t.datetime :starts_at
      t.float :lat
      t.float :lng

      t.string :prestations, array: true

      t.timestamps
    end
  end
end
