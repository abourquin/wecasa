class CreatePrestations < ActiveRecord::Migration[5.2]
  def change
    create_table :prestations, { id: false } do |t|
      t.string :reference, primary_key: true
      t.float :duration

      t.timestamps
    end
  end
end
