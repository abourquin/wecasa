Rails.application.routes.draw do
  root to: 'bookings#new'

  resources :bookings, only: [:new, :edit, :create, :update]
end
