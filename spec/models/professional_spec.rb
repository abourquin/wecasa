require 'rails_helper'

RSpec.describe Professional, type: :model do
  describe '#handle_prestations?' do
    subject { FactoryGirl.create(:professional) }

    context 'when booking match the prestations' do
      let(:booking) { FactoryGirl.create(:booking, prestations: ['woman_haircut']) }

      it 'returns true' do
        expect(subject.handle_prestations?(booking.prestations)).to eq true
      end
    end

    context 'when booking does not match the prestations' do
      let(:booking) { FactoryGirl.create(:booking, prestations: ['man_haircut']) }

      it 'returns false' do
        expect(subject.handle_prestations?(booking.prestations)).to eq false
      end
    end
  end
end
