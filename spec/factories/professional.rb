FactoryGirl.define do
  factory :professional do
    name 'Nathalie"'
    address '22 Av de Friedland 75008 Paris'
    lat 48.874853
    lng 2.300370
    max_kilometers 5
    prestations %w[woman_shampoo woman_haircut woman_brushing]
    opening_hours [{ day: 'monday', starts_at: '8:00', ends_at: '17:00' }]
  end
end
