FactoryGirl.define do
  factory :booking do
    email 'pierre@yopmail.com'
    name  'Pierre Legrand'
    starts_at '2019-08-29T09:30:00+02:00'
    address '73 Avenue de Wagram, 75017 Paris'
    lat 48.879240
    lng 2.298816
    prestations %w[man_haircut]
  end
end
