require 'rails_helper'

RSpec.describe BookingsController, type: :controller do
  describe '#create' do
    subject { -> { post :create, params: params } }
    let(:params) do
      {
        email: 'test@yopmail.com',
        name: 'Test',
        address: 'Test',
        date: '2019-08-28',
        time: '00:00',
        prestations: ['man_haircut']
      }
    end

    context 'when booking is valid' do
      context 'when professional can be found' do
        let(:pro) { FactoryGirl.create(:professional) }
        before(:each) do
          allow(FindProfessionals).to receive(:call).and_return([pro])
        end

        it 'creates a Booking' do
          expect { subject.call }.to change { Booking.count }.by(1)
        end

        it 'redirects to edition page' do
          subject.call
          expect(response).to redirect_to(edit_booking_path(Booking.last.id, pros: [pro]))
          expect(flash[:alert]).to_not be_present
        end
      end

      context 'when professional cannot be found' do
        before(:each) do
          allow(FindProfessionals).to receive(:call).and_return(false)
        end

        it 'prints an alert message' do
          subject.call
          expect(response).to redirect_to(new_booking_path)
          expect(flash[:alert]).to eq 'Cannot find a professional at this date, sorry!'
        end
      end
    end

    context 'when booking is not valid' do
      before(:each) do
        params[:name] = nil
      end

      it 'prints an alert message' do
        subject.call
        expect(response).to redirect_to(new_booking_path)
        expect(flash[:alert]).to be_present
      end
    end
  end
end
