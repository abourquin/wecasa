# rubocop:disable Metrics/BlockLength
ProfessionalPresenter = Struct.new(:professional, :booking) do
  def select?
    handle_prestations? && not_too_far? && open? && available?
  end

  def present
    {
      id: professional.id,
      pro_name: professional.name,
      handle_prestations: handle_prestations?,
      not_too_far: not_too_far?,
      open: open?,
      available: available?,
      selected: select?
    }
  end

  private

  def handle_prestations?
    professional.handle_prestations?(booking.prestations)
  end

  def not_too_far?
    professional.not_too_far?(booking.location)
  end

  def open?
    professional.open?(booking.starts_at, booking.ends_at)
  end

  def available?
    professional.available?(booking.starts_at, booking.ends_at)
  end
end
# rubocop:enable Metrics/BlockLength
