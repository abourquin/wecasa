class Booking < ApplicationRecord
  serialize :prestations, Array
  geocoded_by :address, latitude: :lat, longitude: :lng

  belongs_to :professional, required: false

  validates :email, :name, :address, :prestations, presence: true

  after_validation :geocode

  def ends_at
    @ends_at ||= starts_at + (Prestation.where(reference: prestations).sum(:duration) * 60)
  end

  def location
    @location ||= [lat, lng]
  end
end
