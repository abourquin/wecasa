class Appointment < ApplicationRecord
  belongs_to :professional, dependent: :destroy

  validates :starts_at, :ends_at, presence: true
end
