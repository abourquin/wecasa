class Professional < ApplicationRecord
  serialize :prestations, Array

  has_many :bookings
  has_many :appointments
  accepts_nested_attributes_for :appointments

  def handle_prestations?(booking_prestations)
    (booking_prestations - prestations).blank?
  end

  def open?(starts_at, ends_at)
    return false unless (od = opening_day(starts_at))

    starts_at = starts_at.strftime('%H:%M')
    ends_at = ends_at.strftime('%H:%M')
    Time.parse(starts_at) >= Time.parse(od['starts_at']) &&
      Time.parse(ends_at) <= Time.parse(od['ends_at'])
  end

  def opening_day(starts_at)
    day = starts_at.strftime('%A').downcase
    opening_hours.find { |o| o['day'] == day }
  end

  def not_too_far?(location)
    Geocoder::Calculations.distance_between([lat, lng], location) <= max_kilometers
  end

  def available?(starts_at, ends_at)
    appointments.all? do |a|
      starts_at >= a.ends_at || ends_at <= a.starts_at
    end
  end
end
