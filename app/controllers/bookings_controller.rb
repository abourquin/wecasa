class BookingsController < ApplicationController
  def new
    @booking = Booking.new
  end

  def edit
    @booking = Booking.find(params[:id])
    @professionals = Professional.where(id: params[:pros].map { |p| p[:id] })
  end

  def create
    @booking = Booking.new(booking_param)

    if @booking.valid?
      if (pros = FindProfessionals.call(@booking)).present?
        @booking.save!
        redirect_to edit_booking_path @booking.id, pros: pros
      else
        redirect_to new_booking_path, alert: 'Cannot find a professional at this date, sorry!'
      end
    else
      redirect_to new_booking_path, alert: @booking.errors
    end
  end

  def update
    @booking = Booking.find(params[:id])

    @booking.assign_attributes(booking_update_params)
    # TODO: move some logic to models
    @booking.professional.appointments.create!(
      starts_at: @booking.starts_at, ends_at: @booking.ends_at
    )

    if @booking.save

      redirect_to new_booking_path, notice: 'Your reservation is booked'
    else
      redirect_to new_booking_path, alert: 'Something went wrong, please retry later'
    end
  end

  private

  def booking_param
    params.permit(:email, :name, :address, :date, :time, prestations: []).tap do |p|
      p[:starts_at] = p.delete(:date) + 'T' + p.delete(:time)
    end
  end

  def booking_update_params
    params.require(:booking).permit(:professional_id)
  end
end
