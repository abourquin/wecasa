class FindProfessionals
  attr_reader :booking

  def initialize(booking)
    @booking = booking
  end

  def self.call(booking)
    new(booking).call
  end

  def call
    professionals = Professional.all
    professionals.map do |pro|
      pro = ProfessionalPresenter.new(pro, booking)
      pro.present if pro.select?
    end.compact
  end
end
